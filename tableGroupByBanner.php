<table id="example2-tab1-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Баннеры</th>
            <th>Просмотры</th>
            <th>Клики</th>
            <th>CTR</th>
        </tr>
    </thead>
    <tbody>
        <?php
        require_once 'connect.php';
        $query = $link->query('SELECT v_show_banner_group.banner_id, v_show_banner_group.shows_count, IFNULL(v_click_banner_group.click_count,0) as click_count, banner.name FROM v_show_banner_group
LEFT JOIN v_click_banner_group ON v_show_banner_group.banner_id=v_click_banner_group.banner_id
LEFT JOIN banners_info.banner ON v_show_banner_group.banner_id=banner.id;');
        foreach ($query as $value) {
            $CTR = number_format((($value[click_count] / $value[shows_count]) * 100), 2);
            echo <<< HTML
                <tr>
                  <td>$value[name]</td>
                  <td>$value[shows_count]</td>
                  <td>$value[click_count]</td>
                  <td>$CTR</td>
                </tr>           
HTML;
        }
        ?>
    </tbody>
</table>