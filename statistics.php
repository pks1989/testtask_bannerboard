<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="Pershin K.S.">
        <link rel="icon">
        <title>Тестовое задание</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <link rel="stylesheet" href="css/datatablestab.min.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/datatablestab.min.js"></script>
    </head>

    <body>
        <div class="container">
            <div class="row">
                <form  action="statistics.php" method="post">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary <?php if (isset($_POST['option1'])) echo 'active'; ?>">
                            <input type="radio" name="option1" onChange="this.form.submit()"> Группировка по баннеру
                        </label>
                        <label class="btn btn-primary <?php if (isset($_POST['option2'])) echo 'active'; ?>">
                            <input type="radio" name="option2" onChange="this.form.submit()"> Группировка по дням
                        </label>
                        <label class="btn btn-primary <?php if (isset($_POST['option3'])) echo 'active'; ?>">
                            <input type="radio"  name="option3" onChange="this.form.submit()"> Группировка по часам
                        </label>
                    </div>
                    <br><br><br>
                </form>
            </div>
            <div class="row">
                <!-- Таб панель -->
                <div>
                    <div role="tabpanel" id="example2-tab1">
                        <?php
                            if      (isset($_POST['option2'])):     require_once 'tableGroupByDay.php';
                            elseif  (isset($_POST['option3'])): require_once 'tableGroupByHour.php';
                            else:                               require_once 'tableGroupByBanner.php';
                            endif;
                        ?>  
                    </div>
                </div>
            </div>
            <br>
        </div>
        <div class="container">
            <form  action="index.php">
                <button type="submit" class="btn btn-primary btn-block"><h4>К панели баннеров</h4></button>  
            </form>
        </div>

        <script>
            $(document).ready(function () {
                $('#example2-tab1-dt').DataTable({
                    responsive: true
                });
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    $($.fn.dataTable.tables(true)).DataTable()
                            .columns.adjust()
                            .responsive.recalc();
                });
            }
            );
        </script>
    </body>
</html>
