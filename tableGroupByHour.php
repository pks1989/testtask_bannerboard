<table id="example2-tab1-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Часы</th>
            <th>Просмотры</th>
            <th>Клики</th>
            <th>CTR</th>
        </tr>
    </thead>
    <tbody>
        <?php
        require_once 'connect.php';
        $query = $link->query('SELECT v_show_hours_group.hours, v_show_hours_group.shows_count, IFNULL(v_click_hours_group.click_count,0) 	as click_count FROM v_show_hours_group 	LEFT JOIN v_click_hours_group ON 	v_show_hours_group.hours=v_click_hours_group.hours;');
        foreach ($query as $value) {
            $CTR = number_format((($value[click_count] / $value[shows_count]) * 100), 2);
            echo <<< HTML
                <tr>
                  <td>$value[hours]</td>
                  <td>$value[shows_count]</td>
                  <td>$value[click_count]</td>
                  <td>$CTR</td>
                </tr>           
HTML;
        }
        ?>
    </tbody>
</table>