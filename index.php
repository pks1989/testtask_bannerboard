<!DOCTYPE html>
<html lang="en">
    <head>

        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        $path_to_images = "images/";
        require_once 'connect.php';

        $query = "SELECT v_show_banner_group.banner_id, v_show_banner_group.shows_count, IFNULL(v_click_banner_group.click_count,0) as click_count, banner.name, banner.filename FROM v_show_banner_group LEFT JOIN v_click_banner_group ON v_show_banner_group.banner_id=v_click_banner_group.banner_id LEFT JOIN banners_info.banner ON v_show_banner_group.banner_id=banner.id;";
        $var = $link->query($query);

        for ($i = 0; $i < $var->num_rows; $i++) {
            $tableinarray[] = mysqli_fetch_array($var);
        }
        // $tableinarray = mysqli_fetch_all($var);
        //считаем CRT
        for ($i = 0; $i < count($tableinarray); ++$i) {
            $CTR = number_format((($tableinarray[$i][2] / $tableinarray[$i][1]) * 100), 2);
            $tableinarray[$i][5] = $CTR;
        }
        //сортируем массив по CTR по убыванию.  
        usort($tableinarray, function($a, $b) {
            if ($a['5'] === $b['5'])
                return 0;
            return $a['5'] < $b['5'] ? 1 : -1;
        });
        //заполняем ХИТ-квоту. В квоте у нас 5 мест. Проверяем первые 5 записей и сравниваем их с 6ой. Если запись из топ-5 больше шестой 
        //то всё впорядке, если равна, значит она не может считаться хитом и выборка хитов заканчивается. Потому что хитов у нас может и не быть.
        $hitnumber = 5;
        $showtable;
        for ($i = 0; $i < $hitnumber; ++$i) {
            if ($tableinarray[$i][5] > $tableinarray[$hitnumber][5]) {
                $showtable[] = $tableinarray[$i];
            }
        }
        //Запоминаем какие баннеры попали в хиты
        $usedbanners;
        foreach ($showtable as $var) {
            $usedbanners[] = $var[0];
        }
        $randnum;

        //снова сортируем массив, что бы ссылаться на номера баннеров.  
        usort($tableinarray, function($a, $b) {
            if ($a['0'] === $b['0'])
                return 0;
            return $a['0'] > $b['0'] ? 1 : -1;
        });

        //генерируем случайные числа, если их ещё не было, записываем баннер в таблицу отображения
        for ($i = count($showtable); $i < 10; $i++) {
            do {
                $randnum = rand(1, 15);
                $boolNOTunique = in_array($randnum, $usedbanners);
            } while ($boolNOTunique == true);

            $usedbanners[] = $randnum;
            $showtable[] = $tableinarray[$randnum - 1];
        }

        if (isset($_POST['id']) && isset($_POST['current_datetime'])) {
            $insert = 'INSERT `banners_info`.`banner_events_history` SET `banner_id`=' . mysqli_real_escape_string($link, $_POST['id']) . ', `event_id`=2, `event_datetime`="' . mysqli_real_escape_string($link, $_POST['current_datetime']) . '"';
            echo $link->query($insert);
        }
        ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon">
        <title>Тестовое задание</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12 main" id="banners">
                    <?php
                    foreach ($showtable as $value) {
                        $current_datetime = date('Y-m-d H:i:s');
                        $query = 'INSERT banners_info.banner_events_history SET banner_id="' . mysqli_real_escape_string($link, $value[0]) . '", event_id="1", event_datetime="' . mysqli_real_escape_string($link, $current_datetime) . '"';
                        $link->query($query);
                        echo <<< HTML
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 ">
                      <form action="index.php" method="post">         
                        <input type="hidden" name="id"  value="$value[0]" >
                        <input type="hidden" name="current_datetime"  value="$current_datetime" >
                        <input class="center-block" type="image" src="$path_to_images$value[4].jpg"  alt="OK" >
                        <h4 align="center">$value[3]</h4>
                        <br>
                      </form>
                    </div>                    
HTML;
                    }
                    ?>
                </div>
            </div>
        </div>

        <div>
            <div class="container">
                <form  action="statistics.php" method="post">
                    <input type="hidden" name="option1" value="1" >
                    <button type="submit" class="btn btn-primary btn-block"><h4>К статистике</h4></button>  
                </form>
            </div>
        </div>

    </body>
</html>
